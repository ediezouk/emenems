# React Redux Router example

## Install
```
npm install
```

## Local
```
npm start
```

## Production
```
npm build
```
### Lint
```
npm run build:lint
```
