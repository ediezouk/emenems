import React from 'react';

export default function alertDecorator() {
  const HOC = (Component) => class extends React.Component {
    constructor(props) {
      super(props);
    }

    alert(msg) {
      alert(msg);
    }

    render() {
      return <Component {...this.props} alert={this.alert.bind(this)} />;
    }
  };

  return HOC;
}
