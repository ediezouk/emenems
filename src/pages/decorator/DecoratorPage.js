import React, {Component} from 'react';
import alertDecorator from './alertDecorator';
import ojebDecorator from './ojebDecorator';

@alertDecorator()
@ojebDecorator
class DecoratorPage extends Component {
  componentDidUpdate() {
    this.props.alert('update');
    this.ojebAlert('funguje');
  }

  componentDidMount() {
    this.props.alert('mount');
    this.ojebAlert('funguje');
  }

  render() {
    return (
      <div className='container'>
        <h1>Alert decorator</h1>
        <p>Paragraph</p>
      </div>
    );
  }
}

export default DecoratorPage;
