import React, {Component} from 'react';
import {connect} from 'react-redux';
import { testAction } from '../../actions/test';

import './home.css';

class Home extends Component {
  constructor(props, context) {
    super(props, context);
  }

  handleClick(e) {
    e.preventDefault();
    this.props.dispatch(testAction());
  }

  render() {
    return (
      <div className='container'>
        <h1>Homepage</h1>
        <p>paragraph</p>
        <p>{this.props.test}</p>
        <button onClick={this.handleClick.bind(this)}>Increment</button>
      </div>
    );
  }
}

Home.propTypes = {
  dispatch: React.PropTypes.func,
  test: React.PropTypes.number
};

export default connect(
  (state) => {
    return { test: state.test };
  }
)(Home);
