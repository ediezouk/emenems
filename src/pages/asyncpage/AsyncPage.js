import React, {Component} from 'react';
import {connect} from 'react-redux';

import { testAsyncAction, testClearAsync } from '../../actions/test';

export default class AsyncPage extends Component {
  handleClick(e) {
    e.preventDefault();
    this.props.dispatch(testAsyncAction());
  }

  handleClear(e) {
    e.preventDefault();
    this.props.dispatch(testClearAsync());
  }

  render() {
    // console.log('props', this.props.testAsync);
    return (
      <div className='container'>
        <h1>Async</h1>
        <p>Data</p>
        <p><button onClick={this.handleClick.bind(this)}>Async data</button> <button onClick={this.handleClear.bind(this)}>Clear</button></p>
        <p>{typeof this.props.testAsync.data !== 'undefined' &&
              typeof this.props.testAsync.data[0] !== 'undefined' &&
                typeof this.props.testAsync.data[0].question !== 'undefined' ?
                  this.props.testAsync.data[0].question : 'no data'
            }
        </p>
      </div>
    );
  }
}

AsyncPage.propTypes = {
  dispatch: React.PropTypes.func,
  testAsync: React.PropTypes.object
};

export default connect((state) => {
  return {
    testAsync: state.testAsync
  };
})(AsyncPage);
