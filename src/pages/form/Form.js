import React, {Component} from 'react';
import {reduxForm} from 'redux-form';
// import { Input } from 'react-bootstrap';

import { loadAction } from '../../actions/form';

const initialData = {
  firstname: 'jmeno',
  lastname: 'prijmeni'
};

import './form.css';

class FormPage extends Component {
  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    this.props.dispatch(loadAction(initialData));
  }

  handleSubmit(e) {
    e.preventDefault();
    console.log('handle', this.props.values);
  }

  render() {
    const {fields} = this.props;
    // console.log(firstname);

    return (
      <div className='container'>
        <h1>Form</h1>
        <form>
          <div>
            <label>First Name</label>
            <div>
              <input type='text' placeholder='First Name' {...fields.firstname}/>
            </div>
          </div>
          <div style={{marginBottom: '15', marginTop: '15'}}>
            <label>Last Name</label>
            <div>
              <input type='text' placeholder='Last Name' {...fields.lastname}/>
            </div>
          </div>
          <button onClick={this.handleSubmit.bind(this)}>Submit</button>
        </form>
      </div>
    );
  }
}

FormPage.propTypes = {
  dispatch: React.PropTypes.func,
  fields: React.PropTypes.object.isRequired,
  initialValues: React.PropTypes.object,
  values: React.PropTypes.object
};


export default reduxForm(
  {
    form: 'formName',
    fields: ['firstname', 'lastname']
  },
  (state) => {
    return {
      initialValues: state.initialValues.data
    };
  }
)(FormPage);
