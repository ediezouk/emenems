import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';

export default class Child extends Component {
  componentDidMount() {
    console.log('mount');
  }

  componentDidUpdate() {
    console.log('update');
  }

  render() {
    const { params: { id }} = this.props;

    return (
      <div className='container'>
        <h1>Child</h1>
        <p>Child {id && id}</p>
        <p>
          <Link to='/child'>/child</Link><br/>
          <Link to='/child/10'>/child/10</Link>
        </p>
      </div>
    );
  }
}

Child.propTypes = {
  params: PropTypes.object,
};
