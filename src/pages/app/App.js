import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';

import './app.css';

class App extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='container-fluid'>
        <ul>
          <li><Link to='/'>Home</Link></li>
          <li><Link to='/about'>About</Link></li>
          <li><Link to='/async'>Async</Link></li>
          <li><Link to='/form'>Form</Link></li>
          <li><Link to='/child'>Child</Link></li>
          <li><Link to='/child/1'>Child1</Link></li>
          <li><Link to='/decorator'>Decorator</Link></li>
        </ul>
        <div className='appContent'>
          {this.props.children}
        </div>
      </div>
    );
  }
}

App.propTypes = {
  children: PropTypes.node.isRequired
};

export default App;
