import { LOAD } from '../actions/form';

export function loadData(state = {}, action) {
  switch (action.type) {
  case LOAD:
    return {
      data: action.data
    };
  default:
    return state;
  }
}
