import { TEST, INCREMENT, ASYNC, ASYNC_CLEAR } from '../actions/test';
import objectAssign from 'object-assign';

export function test(state = 0, action) {
  switch (action.type) {
  case TEST:
    return state;
  case INCREMENT:
    return state + 1;
  default:
    return state;
  }
}

export function testAsync(state = {}, action) {
  switch (action.type) {
  case ASYNC:
    return objectAssign({}, {data: action.data});
  case ASYNC_CLEAR:
    return {};
  default:
    return state;
  }
}
