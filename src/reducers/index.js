import { combineReducers } from 'redux';
import { routerStateReducer as router } from 'redux-router';
import { reducer as formReducer } from 'redux-form';
import { test, testAsync } from './test';
import { loadData } from './form';

const rootReducer = combineReducers({
  router,
  form: formReducer,
  test,
  testAsync,
  initialValues: loadData
});

export default rootReducer;
