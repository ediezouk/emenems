// import 'babel-core/polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import Root from './root.react';

const rootDiv = document.getElementById('root');

if (typeof __DEV__ !== 'undefined' && __DEV__) {
  const RedBox = require('redbox-react');
  try {
    ReactDOM.render(<Root />, rootDiv);
  } catch (e) {
    ReactDOM.render(<RedBox error={e} />, rootDiv);
  }
} else {
  ReactDOM.render(<Root />, rootDiv);
}
