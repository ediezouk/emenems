import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { ReduxRouter } from 'redux-router';

import configureStore from './store/configureStore';
import DevTools from './components/DevTools';

import './index.css';

const store = configureStore();

export default class Root extends Component {
  render() {
    return (
      <div>
        <Provider store={store}>
          <div>
            <ReduxRouter />
            {__DEV__ ?
            <DevTools /> : ''}
          </div>
        </Provider>
      </div>
    );
  }
}
