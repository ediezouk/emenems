import React from 'react';
import { IndexRoute, Route } from 'react-router';

import App from '../pages/app/App';
import Home from '../pages/home/Home';
import About from '../pages/about/About';
import AsyncPage from '../pages/asyncpage/AsyncPage';
import Child from '../pages/child/Child';
import FormPage from '../pages/form/Form';
import DecoratorPage from '../pages/decorator/DecoratorPage';
import NotFound from '../pages/misc/NotFound';

const routes = (
  <Route path='/' component={App}>
    <IndexRoute component={Home}/>
    <Route path='/about' component={About}/>
    <Route path='/async' component={AsyncPage}/>
    <Route path='/form' component={FormPage}/>
    <Route path='/child' component={Child}/>
    <Route path='/child/:id' component={Child}/>
    <Route path='/decorator' component={DecoratorPage}/>

    <Route path='*' component={NotFound}/>
   </Route>
);

export default routes;
