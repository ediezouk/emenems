import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { reduxReactRouter } from 'redux-router';
import { createHistory } from 'history';
import rootReducer from '../reducers/index';
import routes from './routes';

let createStoreWithMiddleware;

if (__DEV__) {
  const DevTools = require('../components/DevTools');
  const persistState = require('redux-devtools').persistState;
  const createLogger = require('redux-logger');
  const logger = createLogger({
    duration: false,
    timestamp: false,
    collapsed: true
  });

  createStoreWithMiddleware = compose(
    applyMiddleware(
      thunkMiddleware,
      logger
    ),
    reduxReactRouter({
      routes,
      createHistory
    }),
    DevTools.instrument(),
    persistState(window.location.href.match(/[?&]debug_session=([^&]+)\b/))
  )(createStore);
} else {
  createStoreWithMiddleware = compose(
    applyMiddleware(
      thunkMiddleware
    ),
    reduxReactRouter({
      routes,
      createHistory
    })
  )(createStore);
}

/**
 * Creates a preconfigured store for this example.
 */
export default function configureStore() {
  const store = createStoreWithMiddleware(rootReducer);

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers/index');
      store.replaceReducer(nextRootReducer);
    });
  }

  return store;
}
