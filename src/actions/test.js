import 'isomorphic-fetch';

export const TEST = 'TEST';
export const INCREMENT = 'INCREMENT';
export const ASYNC = 'ASYNC';
export const ASYNC_CLEAR = 'ASYNC_CLEAR';

export function testAction() {
  return {
    type: INCREMENT
  };
}

export function testAsyncAction() {
  return dispatch => {
    fetch('http://private-fa948-marvinui.apiary-mock.com/questions')
      .then((response) => {
        if (response.status >= 400) {
          throw new Error('Bad response from server');
        } else {
          return response.json();
        }
      })
      .then(data => {
        dispatch({
          type: ASYNC,
          data
        });
      })
      .catch(err => {
        console.log('Error in async actionCreator', err);
      });
  };
}

export function testClearAsync() {
  return {
    type: ASYNC_CLEAR
  };
}
