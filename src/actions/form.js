export const LOAD = 'LOAD';

export function loadAction(data) {
  return {
    type: LOAD,
    data
  };
}
